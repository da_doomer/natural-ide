/**
 * Similarity-based library refactoring.
 */
import type { Program } from "./types";

/**
 * Return a program from the library if a suitable match to the string is
 * found, else return the string.
 */
function expanded_string(line: string, library: Program[]): (string | Program) {
	for(const program of library) {
		if(program.name === line)
			return program;
	}
	return line;
}

/**
 * Expand the given program with respect to the library. The returned program
 * has the same number of lines, but lines are replaced with
 * a program in the library whenever a suitable match is found.
 */
function expanded_program(program: Program, library: Program[]): Program {
	let new_lines: (string | Program)[] = [];

	// Recursively expand each line of the program
	for(const line of program.lines) {
		if(typeof line === 'string') {
			// Base case: strings can be replaced with programs from the library
			let new_line = expanded_string(line, library);
			new_lines.push(new_line);
		} else {
			// Recursive case: programs are recursively expanded
			let new_line = expanded_program(line, library);
			new_lines.push(new_line);
		}
	}

	let new_program = {
		name: program.name,
		lines: new_lines,
	};
	return new_program;
}

export {
	expanded_program,
};
