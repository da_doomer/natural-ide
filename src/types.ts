/**
 * Type definitions.
 */

/**
 * A program is recursively defined as a titled sequence of strings or
 * programs.
 */
type Program = {
	name: string,
	lines: (Program | string)[],
};

export type {
	Program,
};
