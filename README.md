# natural-ide

Extremely simple proof of concept of a natural language programming IDE for
building libraries of natural language programs.

A simple refactoring algorithm expands function calls with new matching
function definitions.

## Live demo

A [live demo](https://da_doomer.gitlab.io/natural-ide/) is available.

![screenshot](public/screenshot.png)

## Development instructions

To start a local server run:

```bash
npm run install
npm run dev
```

## Deployment instructions

An example Gitlab CI script ([.gitlab-ci.yml](.gitlab-ci.yml)) is provided.
